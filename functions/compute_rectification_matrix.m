% H = compute_rectification_matrix(points1, points2)
%
% Method: Determines the mapping H * points1 = points2
% 
% Input: points1, points2 of the form (4,n) 
%        n has to be at least 5
%
% Output:  H (4,4) matrix 
% 

function H = compute_rectification_matrix( points1, points2 )

%------------------------------
% TODO: FILL IN THIS PART

    N = numel(points1(1,:));
    x = points1(1,:)';
    y = points1(2,:)';
    z = points1(3,:)';
    w = points1(4,:)';
    xhat = points2(1,:)';
    yhat = points2(2,:)';
    zhat = points2(3,:)';
    what = points2(4,:)';
    
    % Filling of alpha, beta and gamma, similar to when we calculated a 2D
    % homography
    alpha = [x, y, z, w, zeros(N,8), -x .* xhat, -y .* xhat, -z .* xhat, -w .* xhat];
    beta =  [zeros(N,4), x, y, z, w, zeros(N,4), -x .* yhat, -y .* yhat, -z .* yhat, -w .* yhat];
    gamma =  [zeros(N,8), x, y, z, w,  -x .* zhat, -y .* zhat, -z .* zhat, -w .* zhat];
    
    % Construction of W as alpha stacked on beta, stacked on gamma.
    % Dimensions: 3Nx16
    W = [alpha; beta; gamma];
    
    % Singular value decomposition of W
    [U, S, V] = svd(W);
    
    % The homography h (vector form) estimated as the vector V of the SVD with the smallest
    % associated singular value
    h = V(:, end);
    
    % Reshaping of the vector h into the homography matrix H
    H = (reshape(h, [4, 4]) / h(end))';

end