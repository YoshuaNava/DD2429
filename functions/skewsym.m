function [ matrix ] = skewsym( vector )
%SKEWSYM Skew symmetric matrix of a 3x1 vector
%   This function receives a 3x1 vector and returns a 3x3
%skew-symmetric matrix

    matrix = [0, -vector(3), vector(2);
              vector(3), 0, -vector(1);
              -vector(2), vector(1), 0];

end

