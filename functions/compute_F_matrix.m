% function F = compute_F_matrix(points1, points2);
%
% Method:   Calculate the F matrix between two views from
%           point correspondences: points2^T * F * points1 = 0
%           We use the normalize 8-point algorithm and 
%           enforce the constraint that the three singular 
%           values are: a,b,0. The data will be normalized here. 
%           Finally we will check how good the epipolar constraints:
%           points2^T * F * points1 = 0 are fullfilled.
% 
%           Requires that the number of cameras is C=2.
% 
% Input:    points2d is a 3xNxC array storing the image points.
%
% Output:   F is a 3x3 matrix where the last singular value is zero.

function F = compute_F_matrix( points2d )


%------------------------------
% TODO: FILL IN THIS PART

    % Calculate number of points provided
    N = numel(points2d(1, :, 1));
    
    % Re-organize points given in vectors p_a and p_b
    p_a = points2d(:,:,1);
    p_b = points2d(:,:,2);
    
    % Normalization of points p_a and p_b centers
    norm_mat_a = compute_normalization_matrices(p_a);
    norm_mat_b = compute_normalization_matrices(p_b);
    p_a = norm_mat_a * p_a;
    p_b = norm_mat_b * p_b;

    % Coordinates of points p_a and p_b
    x_a = p_a(1, 1:N)';
    y_a = p_a(2, 1:N)';
    x_b = p_b(1, 1:N)';
    y_b = p_b(2, 1:N)';

    % Vector W. This is part of the quadratic cost expression W*e=0
    W = [x_b .* x_a, x_b .* y_a, x_b, y_b .* x_a, y_b .* y_a, y_b, x_a, y_a, ones(N,1)];
    
    % Singular value decomposition of W
    [U, S, V] = svd(W);
    
    % The essential matrix e (vector form) estimated as the vector V of the SVD with the smallest
    % associated singular value
    f = V(:, end);
    
    % Reshaping of the vector e into matrix F
    F = reshape(f,[3,3])';
    
    % Get (almost) E from F
    F = norm_mat_b' * F * norm_mat_a;
    
    % Fix F so it fulfills the requirements
    [U_F, S_F, V_F] = svd(F);
    F = U_F * diag([S_F(1,1), S_F(2,2), 0]) * V_F';
    
%     % Calculation of fundamental matrix error, by checking the epipolar
%     constraint
%     error = zeros(1, N);
%     for i=1:N
%         error(i) = p_b(:,i)' * F * p_a(:,i);
%     end
%     error

end