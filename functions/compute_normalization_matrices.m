% Method:   compute all normalization matrices.  
%           It is: point_norm = norm_matrix * point. The norm_points 
%           have centroid 0 and average distance = sqrt(2))
% 
%           Let N be the number of points and C the number of cameras.
%
% Input:    points2d is a 3xNxC array. Stores un-normalized homogeneous
%           coordinates for points in 2D. The data may have NaN values.
%        
% Output:   norm_mat is a 3x3xC array. Stores the normalization matrices
%           for all cameras, i.e. norm_mat(:,:,c) is the normalization
%           matrix for camera c.

function norm_mat = compute_normalization_matrices( points2d )

%-------------------------
% TODO: FILL IN THIS PART

    % Pre-alocate memory for matrix
    norm_mat = zeros(3, 3, length(points2d(1,1,:)));

    % Iterate over cameras and calculate norm_mat
    for c = 1:length(points2d(1,1,:))

        % Calculate center point
        validPoints = ~isnan(points2d(:,:,c));
        Xc = mean(points2d(1,validPoints(1,:),c)); 
        Yc = mean(points2d(2,validPoints(2,:),c));

        % Calculate average distance to center point
        X = Xc - points2d(1,validPoints(1,:),c);
        Y = Yc - points2d(2,validPoints(2,:),c);
        dc = mean(sqrt(X.^2 + Y.^2));

        % Calculate the values of the normalization matrix
        norm_mat(:,:,c) = (sqrt(2) / dc) * [1,0,-Xc;
                                            0,1,-Yc;
                                            0,0,dc/sqrt(2)];
    end
end