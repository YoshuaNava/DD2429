% H = compute_homography(points1, points2)
%
% Method: Determines the mapping H * points1 = points2
% 
% Input:  points1, points2 are of the form (3,n) with 
%         n is the number of points.
%         The points should be normalized for 
%         better performance.
% 
% Output: H 3x3 matrix 
%
% Team members:
%    Niklas Rolleberg
%    Yoshua Nava
%

function H = compute_homography( points1, points2 )

%-------------------------
% TODO: FILL IN THIS PART
    % Find number and location of valid points inside the point arrays
    valid_indices = find(~isnan(points1(1,:)) & ~isnan(points2(2,:)));
    
    
    N = numel(valid_indices);

    % Filling of alpha and beta, following the procedure from the lecture
    % notes. Dimensions: Nx9
    x_a = points2(1,valid_indices)';
    y_a = points2(2,valid_indices)';
    x_b = points1(1,valid_indices)';
    y_b = points1(2,valid_indices)';
    alpha = [x_b, y_b, ones(N,1), zeros(N,1), zeros(N,1), zeros(N,1), -x_b.*x_a, -y_b.*x_a, -x_a];
    beta = [zeros(N,1), zeros(N,1), zeros(N,1), x_b, y_b, ones(N,1), -x_b.*y_a, -y_b.*y_a, -y_a];

    % Construction of Q as alpha stacked over beta. Dimensions: 2Nx9
    Q = [alpha; beta];
    
    % Singular value decomposition of Q
    [U, S, V] = svd(Q);
    
    % The homography h (vector form) estimated as the vector V of the SVD with the smallest
    % associated singular value
    h = V(:, end);
    
    % Reshaping of the vector h into the homography matrix H
    H = (reshape(h, [3, 3]) / h(end))';
    
end