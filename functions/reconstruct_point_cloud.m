% function model = reconstruct_point_cloud(cam, data)
%
% Method:   Determines the 3D model points by triangulation
%           of a stereo camera system. We assume that the data 
%           is already normalized 
% 
%           Requires that the number of cameras is C=2.
%           Let N be the number of points.
%
% Input:    points2d is a 3xNxC array, storing all image points.
%
%           cameras is a 3x4xC array, where cameras(:,:,1) is the first and 
%           cameras(:,:,2) is the second camera matrix.
% 
% Output:   points3d 4xN matrix of all 3d points.


function points3d = reconstruct_point_cloud( cameras, points2d )

%------------------------------
% TODO: FILL IN THIS PART

    % Calculation of number of points provided, and construction of output
    % array points3d
    N = numel(points2d(1,:,1));
    points3d = zeros(4, N);

    % Extraction of M_a and M_b from camera matrices array
    M_a = cameras(:,:,1);
    M_b = cameras(:,:,2);
    
    % Loop over all the points. For each pair of 2D points seen by camera a and b,
    % compute the original 3D point that was observed
    for i=1:N
        x_a = points2d(1, i, 1);
        y_a = points2d(2, i, 1);
        x_b = points2d(1, i, 2);
        y_b = points2d(2, i, 2);

        W = [x_a*M_a(3,1) - M_a(1,1), x_a*M_a(3,2) - M_a(1,2), x_a*M_a(3,3) - M_a(1,3), x_a*M_a(3,4) - M_a(1,4);
             y_a*M_a(3,1) - M_a(2,1), y_a*M_a(3,2) - M_a(2,2), y_a*M_a(3,3) - M_a(2,3), y_a*M_a(3,4) - M_a(2,4);
             x_b*M_b(3,1) - M_b(1,1), x_b*M_b(3,2) - M_b(1,2), x_b*M_b(3,3) - M_b(1,3), x_b*M_b(3,4) - M_b(1,4);
             y_b*M_b(3,1) - M_b(2,1), y_b*M_b(3,2) - M_b(2,2), y_b*M_b(3,3) - M_b(2,3), y_b*M_b(3,4) - M_b(2,4)];

        [U, S, V] = svd(W);
        P = V(:,end) / V(end,end);
        points3d(:,i) = P;
    end    

end