% function [cams, cam_centers] = reconstruct_uncalibrated_stereo_cameras(F); 
%
% Method: Calculate the first and second uncalibrated camera matrix
%         from the F-matrix. 
% 
% Input:  F - Fundamental matrix with the last singular value 0 
%
% Output:   cams is a 3x4x2 array, where cams(:,:,1) is the first and 
%           cams(:,:,2) is the second camera matrix.
%
%           cam_centers is a 4x2 array, where (:,1) is the first and (:,2) 
%           the second camera center.

function [cams, cam_centers] = reconstruct_uncalibrated_stereo_cameras( F )


%------------------------------
% TODO: FILL IN THIS PART

 % Creation of a skew-symmetric matrix S from a vector v
 v = [1; 1; 1];
 S = skewsym(v);

 % Projective matrix for camera A
 M_a = [eye(3), zeros(3,1)];
 
 % Projective matrix for camera B
 % Reference: https://www.cs.auckland.ac.nz/courses/compsci773s1t/lectures/773-GGpdfs/773GG-FundMatrix-A.pdf
 % Left epipole e_l: V(:,end)
 % Right epipole e_r: U(:,end)
 [U_F, S_F, V_F] = svd(F);
 h = U_F(:,end);
 if(h(3) ~= 0)
    h = h / h(3);
 end
 H = (S * F);
 M_b = [H, h];
 
 % Extraction of camera centers t_x from the projection matrices M_x
 [U_a, S_a, V_a] = svd(M_a);
 t_a = V_a(:,end);
 [U_b, S_b, V_b] = svd(M_b);
 t_b = V_b(:,end);

 % Setting up the function outputs
 cam_centers = zeros(4, 2);
 cam_centers(:,1) = t_a;
 cam_centers(:,2) = t_b;
 
 cams = zeros(3, 4, 2);
 cams(:,:,1) = M_a;
 cams(:,:,2) = M_b;
 
end