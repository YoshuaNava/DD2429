% function [cams, cam_centers] = reconstruct_stereo_cameras(E, K1, K2, data); 
%
% Method:   Calculate the first and second camera matrix. 
%           The second camera matrix is unique up to scale. 
%           The essential matrix and 
%           the internal camera matrices are known. Furthermore one 
%           point is needed in order to solve the ambiguity in the 
%           second camera matrix.
%
%           Requires that the number of cameras is C=2.
%
% Input:    E is a 3x3 essential matrix with the singular values (a,a,0).
%
%           K is a 3x3xC array storing the internal calibration matrix for
%           each camera.
%
%           points2d is a 3xC matrix, storing an image point for each camera.
%
% Output:   cams is a 3x4x2 array, where cams(:,:,1) is the first and 
%           cams(:,:,2) is the second camera matrix.
%
%           cam_centers is a 4x2 array, where (:,1) is the first and (:,2) 
%           the second camera center.
%

function [cams, cam_centers] = reconstruct_stereo_cameras( E, K, points2d )

%------------------------------
% TODO: FILL IN THIS PART

    % Singular value decomposition of essential matrix E
    [U, S, V] = svd(E);
    
    % Camera b translation as the eigenvector associated to the smallest
    % singular value, omega=0
    t = V(:, end);
    
    % Construction of the 2 rotation matrix hypothesis
    W = [0, -1, 0; 1, 0, 0; 0, 0, 1];
    R_1 = U * W * V';
    R_1 = R_1 * det(R_1);
    R_2 = U * W' * V';
    R_2 = R_2 * det(R_2);
    
    % Construction of M_a, and the four hypothesis on M_b
    M_a = K(:,:,1) * [eye(3,3), zeros(3,1)];
    M_b = zeros(3, 4, 4);
    M_b(:, :, 1) = K(:,:,2) * R_1*[eye(3), -t];
    M_b(:, :, 2) = K(:,:,2) * R_1*[eye(3), t];
    M_b(:, :, 3) = K(:,:,2) * R_2*[eye(3), -t];
    M_b(:, :, 4) = K(:,:,2) * R_2*[eye(3), t];
    
    
    % Loop to find which of our hypothesis on M_b is correct
    cams = zeros(3, 4, length(points2d(1,1,:)));
    cams(:, :, 1) = M_a;
    for i=1:4
        cams(:, :, 2) = M_b(:, :, i);
        points3d = reconstruct_point_cloud(cams, points2d);
        
        p3d_a = M_a * points3d;
        p3d_b = M_b(:,:,i) * points3d;
        if((p3d_a(3) > 0) && (p3d_b(3) > -t(3)))
            break;
        end
    end

    
    % Camera centers definition. Camera a is at the origin, camera b is
    % translated -t units
    cam_centers = [[zeros(3,1);1], [-t;1]];
end

